import 'dart:io';

import 'package:audioplayers/audioplayers.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
	// This widget is the root of your application.
	@override
	Widget build(BuildContext context) {
		return MaterialApp(
			title: 'Audio Player',
			theme: ThemeData(
				// This is the theme of your application.
				//
				// Try running your application with "flutter run". You'll see the
				// application has a blue toolbar. Then, without quitting the app, try
				// changing the primarySwatch below to Colors.green and then invoke
				// "hot reload" (press "r" in the console where you ran "flutter run",
				// or simply save your changes to "hot reload" in a Flutter IDE).
				// Notice that the counter didn't reset back to zero; the application
				// is not restarted.
				primarySwatch: Colors.indigo,
				scaffoldBackgroundColor: Colors.white
			),
			home: MyHomePage(title: 'Audio Player'),
		);
	}
}

class MyHomePage extends StatefulWidget {
	MyHomePage({Key key, this.title}) : super(key: key);

	// This widget is the home page of your application. It is stateful, meaning
	// that it has a State object (defined below) that contains fields that affect
	// how it looks.

	// This class is the configuration for the state. It holds the values (in this
	// case the title) provided by the parent (in this case the App widget) and
	// used by the build method of the State. Fields in a Widget subclass are
	// always marked "final".

	final String title;

	@override
	_MyHomePageState createState() => _MyHomePageState();
}

class AudioItem {
	String path;
	bool play;

	AudioItem({this.path, this.play});
}

class _MyHomePageState extends State<MyHomePage> {
	List<int> playersStatus = new List();
	Map<String, dynamic> items = new Map();
	AudioPlayer audioPlayer = AudioPlayer();
	Duration maxDuration;
	Duration currentPosition;
	int currentAudio;

	@override
	void initState() {
		super.initState();

		initData();
	}

	void initData() async {
		SharedPreferences prefs = await SharedPreferences.getInstance();

		for (int i = 0; i < 20; i++) {
			String path = prefs.getString('path' + i.toString());

			setState(() {
				prefs.setString(
					'path' + i.toString(), (path != null) ? path : '');
				playersStatus.insert(i, 0);
				items['path' + i.toString()] = (path != null) ? path : '';
			});
		}

		audioPlayer.onDurationChanged.listen((Duration d) {
			setState(() => maxDuration = d);
		});

		audioPlayer.onAudioPositionChanged.listen((Duration d) {
			setState(() => currentPosition = d);
		});

		audioPlayer.onPlayerCompletion.listen((event) {
			stopAudio(currentAudio);

			setState(() {
				currentPosition = Duration.zero;
				maxDuration = Duration(seconds: 1);
			});
		});
	}

	@override
	Widget build(BuildContext context) {
		// This method is rerun every time setState is called, for instance as done
		// by the _incrementCounter method above.
		//
		// The Flutter framework has been optimized to make rerunning build methods
		// fast, so that you can just rebuild anything that needs updating rather
		// than having to individually change instances of widgets.
		return Scaffold(
			appBar: AppBar(
				// Here we take the value from the MyHomePage object that was created by
				// the App.build method, and use it to set our appbar title.
				title: Text(widget.title),
				elevation: 4,
			),
			body: Center(
				// Center is a layout widget. It takes a single child and positions it
				// in the middle of the parent.
				child: items.length > 0 ?
				ListView.builder(
					itemBuilder: (BuildContext context, int index) {
						return cardDisplay(index);
					},
					itemCount: 20, //list.items.length,
				) : CircularProgressIndicator(),
			),
//			floatingActionButton: FloatingActionButton(
//				backgroundColor: Colors.redAccent,
//				child: Icon(Icons.add),
//			),
		);
	}

	Widget cardDisplay(int index) {
		return Card(
			elevation: 0,
			margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
			shape: RoundedRectangleBorder(
				borderRadius: BorderRadius.all(Radius.circular(0)),
			),
			color: playersStatus.elementAt(index) > 0 ? Colors.black45 : Colors
				.indigo,
			child: Container(
				padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
				child: Row(
					children: <Widget>[
						Expanded(
							flex: 2,
							child: indexDisplay(index)
						),
						SizedBox(width: 10,),
						playButton(index),
//						pauseButton(index),
						stopButton(index),
					],
				),
			),
		);
	}

	Widget indexDisplay(int index) {
		String path = items['path' + index.toString()];
		return GestureDetector(
			onTap: () {
				pickAudio(index);
			},
			child: Row(
				mainAxisSize: MainAxisSize.min,
				mainAxisAlignment: MainAxisAlignment.center,
				crossAxisAlignment: CrossAxisAlignment.center,
				children: <Widget>[
					CircleAvatar(
						radius: 30,
						child: Stack(
							alignment: Alignment.center,
							children: <Widget>[
								SizedBox(
									width: 55,
									height: 55,
									child:
									((playersStatus.elementAt(index) == 1 ||
										playersStatus.elementAt(index) == 2) &&
										currentPosition != null &&
										maxDuration != null) ?
									RotatedBox(
										quarterTurns: 1,
										child: CircularProgressIndicator(
											value: (currentPosition.inSeconds /
												maxDuration.inSeconds),
											strokeWidth: 5,
											valueColor: new AlwaysStoppedAnimation<
												Color>(Colors.pinkAccent),
										),
									) : Container(),
								),

								(playersStatus.elementAt(index) == 1) ?
								pauseButton(index) : Container(),

								(playersStatus.elementAt(index) == 2) ?
								resumeButton(index) : Container(),

								(playersStatus.elementAt(index) == 0) ?
								Text(
									(index + 1).toString(),
									style: TextStyle(
										color: Colors
											.white70,
										fontSize: 20,
										fontWeight: FontWeight.bold,
									),
								) : Container(),

								(playersStatus.elementAt(index) == 0) ?
								Icon(
									Icons.audiotrack,
									color: Colors.white12,
									size: 55,
								) : Container(),
							]
						),
						backgroundColor: playersStatus.elementAt(index) > 0 ? Colors.white : Colors.white12,
					),
					SizedBox(width: 20,),
					Expanded(
						child: Text(
							(path != null && path != '')
								? path
								: 'Pick Audio Track',
							style: TextStyle(
								color: Colors.white,
								fontSize: 12,
							),
						),
					),
				],
			),
		);
	}

	Widget resumeButton(int index) {
		return
			(playersStatus.elementAt(index) == 2 &&
				items['path' + index.toString()] != '') ?
			GestureDetector(
				onTap: () {
					resumeAudio(index);
				},
				child: Icon(
					Icons.play_circle_filled,
					color: Colors.indigo,
					size: 60,
				),
			) : Container();
	}

	Widget playButton(int index) {
		return
			(playersStatus.elementAt(index) == 0 &&
				items['path' + index.toString()] != '') ?
			GestureDetector(
				onTap: () {
					playAudio(index);
				},
				child: Icon(
					Icons.play_circle_filled,
					color: Colors.white60,
					size: 60,
				),
			) : Container();
	}

	Widget pauseButton(int index) {
		return (playersStatus.elementAt(index) == 1) ?
		GestureDetector(
			onTap: () {
				pauseAudio(index);
			},
			child: Icon(
				Icons.pause_circle_filled,
				color: Colors.indigo,
				size: 60,
			),
		) : Container();
	}

	Widget stopButton(int index) {
		return (playersStatus.elementAt(index) == 1 ||
			playersStatus.elementAt(index) == 2) ?
		GestureDetector(
			onTap: () {
				stopAudio(index);
			},
			child: Icon(
				MdiIcons.stopCircle,
				color:
				Colors.white,
				size: 60,
			),
		) : Container();
	}

	void pickAudio(int index) async {
		SharedPreferences prefs = await SharedPreferences.getInstance();

		File file = await FilePicker.getFile(type: FileType.AUDIO);

		setState(() {
			prefs.setString('path' + index.toString(), file.path);
			items['path' + index.toString()] = file.path;
		});
	}

	void playAudio(int index) async {
		await audioPlayer.stop();

		setState(() {
			playersStatus.fillRange(0, playersStatus.length, 0);
			playersStatus.insert(index, 1);
			currentAudio = index;
		});

		String path = items['path' + index.toString()];
		await audioPlayer.play(path, isLocal: true);
	}

	void stopAudio(int index) async {
		setState(() {
			playersStatus.fillRange(0, playersStatus.length, 0);
			playersStatus.insert(index, 0);
			currentAudio = index;
		});

		await audioPlayer.stop();
	}

	void pauseAudio(int index) async {
		setState(() {
			playersStatus.fillRange(0, playersStatus.length, 0);
			playersStatus.insert(index, 2);
			currentAudio = index;
		});

		await audioPlayer.pause();
	}

	void resumeAudio(int index) async {
		setState(() {
			playersStatus.fillRange(0, playersStatus.length, 0);
			playersStatus.insert(index, 1);
			currentAudio = index;
		});

		await audioPlayer.resume();
	}
}
